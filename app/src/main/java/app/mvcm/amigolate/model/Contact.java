package app.mvcm.amigolate.model;


public class Contact {
    private static long id;
    private static String name;
    private static String email;
    public static String avatar = "letter";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
