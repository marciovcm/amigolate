package app.mvcm.amigolate.controller.database;


import android.database.sqlite.SQLiteDatabase;

public class HistoryTable {

    // Database table
    public static final String TABLE_HISTORY = "history";
    public static final String COLUMN_HISTORY_ID = "_id";
    public static final String COLUMN_HISTORY_DRAW_ID = "draw_id";
    public static final String COLUMN_HISTORY_CONTACT_ID = "contact_id";

    // Database creation sql statement
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_HISTORY
            + "( "
            + COLUMN_HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_HISTORY_DRAW_ID + " INTEGER, FOREIGN KEY (draw_id) REFERENCES draw(_id), "
            + COLUMN_HISTORY_CONTACT_ID + " INTEGER, FOREIGN KEY (contact_id) REFERENCES contacts(_id)"
            + " )";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        onCreate(database);
    }
}
