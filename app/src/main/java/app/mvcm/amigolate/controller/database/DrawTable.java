package app.mvcm.amigolate.controller.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by m.motta on 23/12/2016.
 */

public class DrawTable {

    // Database table
    public static final String TABLE_DRAW = "draw";
    public static final String COLUMN_DRAW_ID = "_id";
    public static final String COLUMN_DRAW_TITLE = "draw_title";
    public static final String COLUMN_DRAW_SUMMARY = "draw_summary";

    // Database creation sql statement
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_DRAW
            + "( "
            + COLUMN_DRAW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_DRAW_TITLE + " TEXT NOT NULL, "
            + COLUMN_DRAW_SUMMARY + " TEXT NOT NULL"
            + " )";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_DRAW);
        onCreate(database);
    }

}
