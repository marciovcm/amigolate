package app.mvcm.amigolate.controller.database;


import android.database.sqlite.SQLiteDatabase;

public class ContactTable{

    // Database table and columns
    public static final String TABLE_CONTACTS = "contacts";
    public static final String COLUMN_CONTACT_ID = "_id";
    public static final String COLUMN_CONTACT_NAME = "contact_name";
    public static final String COLUMN_CONTACT_EMAIL = "contact_email";

    public static final int COLUMN_CONTACT_ID_IDX = 0;
    public static final int COLUMN_CONTACT_NAME_IDX = 1;
    public static final int COLUMN_CONTACT_EMAIL_IDX = 2;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_CONTACTS
            + "( "
            + COLUMN_CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CONTACT_NAME + " TEXT NOT NULL, "
            + COLUMN_CONTACT_EMAIL + " TEXT NOT NULL"
            + " )";


    public static void onCreate(SQLiteDatabase db){
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

}
