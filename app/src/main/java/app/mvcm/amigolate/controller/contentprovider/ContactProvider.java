package app.mvcm.amigolate.controller.contentprovider;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

import app.mvcm.amigolate.controller.database.ContactTable;
import app.mvcm.amigolate.controller.database.DBHelper;

public class ContactProvider extends ContentProvider{

    // database
    private DBHelper mDbHelper;

    // used for UriMatcher
    private static final int CONTACTS = 10;
    private static final int CONTACTS_ID = 20;

    private static final String AUTHORITY = "app.mvcm.amigolate";

    private static final String BASE_PATH = "contacts";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, CONTACTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", CONTACTS_ID);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new DBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // set the table
        queryBuilder.setTables(ContactTable.TABLE_CONTACTS);

        int uriType = sURIMatcher.match(uri);
        switch (uriType){
            case CONTACTS:
                break;
            case CONTACTS_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(ContactTable.COLUMN_CONTACT_ID + " = " + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase mSqLiteDatabase = mDbHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(mSqLiteDatabase, projection, selection, selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase mSqLiteDatabase = mDbHelper.getWritableDatabase();
        long id = 0;

        switch (uriType){
            case CONTACTS:
                id = mSqLiteDatabase.insert(ContactTable.TABLE_CONTACTS, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase mSqLiteDatabase = mDbHelper.getWritableDatabase();
        int rowsDeleted = 0;

        switch (uriType){
            case CONTACTS:
                rowsDeleted = mSqLiteDatabase.delete(ContactTable.TABLE_CONTACTS, selection, selectionArgs);
                break;
            case CONTACTS_ID:
                String id = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsDeleted = mSqLiteDatabase.delete(ContactTable.TABLE_CONTACTS, ContactTable.COLUMN_CONTACT_ID + " = " + id, null);
                }else{
                    rowsDeleted = mSqLiteDatabase.delete(ContactTable.TABLE_CONTACTS, ContactTable.COLUMN_CONTACT_ID + " = " + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase mSqLiteDatabase = mDbHelper.getWritableDatabase();
        int rowsUpdated = 0;

        switch (uriType){
            case CONTACTS:
                rowsUpdated = mSqLiteDatabase.update(ContactTable.TABLE_CONTACTS, values, selection, selectionArgs);
                break;
            case CONTACTS_ID:
                String id = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsUpdated = mSqLiteDatabase.update(ContactTable.TABLE_CONTACTS, values, ContactTable.COLUMN_CONTACT_ID + " = " + id, null);
                }else{
                    rowsUpdated = mSqLiteDatabase.update(ContactTable.TABLE_CONTACTS, values, ContactTable.COLUMN_CONTACT_ID + " = " + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    private void checkColumns(String[] projection){

        String[] available = { ContactTable.COLUMN_CONTACT_EMAIL, ContactTable.COLUMN_CONTACT_NAME, ContactTable.COLUMN_CONTACT_ID };

        if(projection != null){
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            if(!availableColumns.containsAll(requestedColumns)){
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }

    }
}
