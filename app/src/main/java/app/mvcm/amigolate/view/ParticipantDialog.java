package app.mvcm.amigolate.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.LayoutInflater;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.mvcm.amigolate.R;
import app.mvcm.amigolate.controller.contentprovider.ContactProvider;
import app.mvcm.amigolate.controller.database.ContactTable;
import app.mvcm.amigolate.model.Contact;


public class ParticipantDialog extends DialogFragment implements View.OnClickListener{

    private EditText et_name;
    private EditText et_email;
    private Button bt_cancel;
    private Button bt_ok;
    private String dialog_tag;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_participant, null);

        et_name = (EditText) view.findViewById(R.id.dialog_participant_name);
        et_email = (EditText) view.findViewById(R.id.dialog_participant_email);
        bt_cancel = (Button) view.findViewById(R.id.dialog_participant_cancel);
        bt_ok = (Button) view.findViewById(R.id.dialog_participant_ok);

        et_name.addTextChangedListener(mTextWatcher);
        et_email.addTextChangedListener(mTextWatcher);

        bt_cancel.setOnClickListener(this);
        bt_ok.setOnClickListener(this);

        builder.setMessage(R.string.dialog_participant_info);
        builder.setView(view);

        dialog_tag = getTag();

        initializeDialog();

        return builder.create();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.dialog_participant_cancel:
                dismiss();
                break;
            case R.id.dialog_participant_ok:
                dismiss();
                getContactInfo();
                break;
        }
    }

    public void getContactInfo(){

        ContentValues values = new ContentValues();
        String name = et_name.getText().toString();
        String email = et_email.getText().toString().toLowerCase();
        Pattern mPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher mMatcher = mPattern.matcher(email);

        if(mMatcher.matches() == false){
            Toast.makeText(getActivity(), getResources().getString(R.string.dialog_participant_invalid_email), Toast.LENGTH_LONG).show();
        }else{
            values.put(ContactTable.COLUMN_CONTACT_NAME, name);
            values.put(ContactTable.COLUMN_CONTACT_EMAIL, email);

            if(dialog_tag.contains("update")){
                Contact contact = new Contact();
                getActivity().getContentResolver().update(ContactProvider.CONTENT_URI, values, "_id = " + contact.getId(), null);
            }else if(dialog_tag.contains("create")){
                Cursor cursor = getActivity().getContentResolver().query(ContactProvider.CONTENT_URI, null, "contact_email = '" + email + "'", null, null);
                if(cursor.getCount() == 0){
                    getActivity().getContentResolver().insert(ContactProvider.CONTENT_URI, values);
                }else{
                    Toast.makeText(getActivity(), getResources().getString(R.string.dialog_participant_email_exists), Toast.LENGTH_LONG).show();
                }
                cursor.close();
            }else if(dialog_tag.contains("add")){

            }
        }
    }

    public void initializeDialog(){
        if(dialog_tag.contains("update")){
            Contact contact = new Contact();
            et_name.setText(contact.getName());
            et_email.setText(contact.getEmail());
        }else if(dialog_tag.contains("create") || dialog_tag.contains("add")){
            et_name.setText("");
            et_email.setText("");
        }
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        private boolean watcher_name = false;
        private boolean watcher_email = false;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if((s == et_name.getEditableText()) && (s.length() > 0)){
                watcher_name = true;
            }else if((s == et_email.getEditableText()) && (s.length() > 0)){
                watcher_email = true;
            }else if((s == et_name.getEditableText()) && (s.length() == 0)){
                watcher_name = false;
            }else if((s == et_email.getEditableText()) && (s.length() == 0)){
                watcher_email = false;
            }

            if((watcher_name == true) && (watcher_email == true)){
                bt_ok.setTextColor(getResources().getColor(R.color.colorAccent));
                bt_ok.setClickable(true);
                bt_ok.setEnabled(true);
            }else{
                bt_ok.setTextColor(getResources().getColor(R.color.hint_text_color));
                bt_ok.setClickable(false);
                bt_ok.setEnabled(false);
            }
        }
    };
}
