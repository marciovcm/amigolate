package app.mvcm.amigolate.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.mvcm.amigolate.R;

public class NewDrawActivity extends AppCompatActivity implements View.OnClickListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdraw);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager mViewPager = (ViewPager) findViewById(R.id.newdraw_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout mTabLayout = (TabLayout) findViewById(R.id.newdraw_tab);
        mTabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.newdraw_fab);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.newdraw_fab:
                ParticipantDialog dialog = new ParticipantDialog();
                dialog.setCancelable(false);
                dialog.show(getFragmentManager(), "ParticipantDialog_add_contact");
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.newdraw_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }

    public static class PlaceholderFragment extends Fragment{

        private static final String ARG_SECTION_NUMBER = "section_number";
        private TextView container_title;
        private TextView container_participants;
        private TextView container_summary;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber){
            PlaceholderFragment mPlaceholderFragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            mPlaceholderFragment.setArguments(args);

            return mPlaceholderFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pager_newdraw, container, false);

            int section = getArguments().getInt(ARG_SECTION_NUMBER);

            container_title = (TextView) rootView.findViewById(R.id.newdraw_container_title);
            container_participants = (TextView) rootView.findViewById(R.id.newdraw_container_participants);
            container_summary = (TextView) rootView.findViewById(R.id.newdraw_container_summary);

            if(section == 1){
                container_title.setVisibility(View.VISIBLE);
                container_participants.setVisibility(View.VISIBLE);
                container_summary.setVisibility(View.GONE);
            }else{
                container_title.setVisibility(View.GONE);
                container_participants.setVisibility(View.GONE);
                container_summary.setVisibility(View.VISIBLE);
            }

            return rootView;
        }
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter{

        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position +1);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
