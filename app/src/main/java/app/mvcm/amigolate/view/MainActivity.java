package app.mvcm.amigolate.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import app.mvcm.amigolate.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout draw_button;
    private RelativeLayout contacts_button;
    private RelativeLayout history_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);

        draw_button = (RelativeLayout) findViewById(R.id.newdraw_button);
        draw_button.setOnClickListener(this);
        contacts_button = (RelativeLayout) findViewById(R.id.contacts_button);
        contacts_button.setOnClickListener(this);
        history_button = (RelativeLayout) findViewById(R.id.history_button);
        history_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;

        switch (id){
            case R.id.newdraw_button:
                DrawDialog dialog = new DrawDialog();
                dialog.setCancelable(false);
                dialog.show(getFragmentManager(), "DrawDialog");
                break;
            case R.id.contacts_button:
                intent = new Intent(this, ContactsActivity.class);
                startActivity(intent);
                break;
            case R.id.history_button:
                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int action = item.getItemId();
        Intent intent;

        switch (action){
            case R.id.main_search:
                intent = new Intent(this, FindActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
