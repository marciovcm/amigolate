package app.mvcm.amigolate.view.adapter;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.TextView;

import app.mvcm.amigolate.R;
import app.mvcm.amigolate.controller.database.ContactTable;
import app.mvcm.amigolate.model.Contact;

public class ContactAdapter extends CursorAdapter{

    public ContactAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder holder = (ViewHolder) view.getTag();

        String contact_name = cursor.getString(ContactTable.COLUMN_CONTACT_NAME_IDX);
        String contact_email = cursor.getString(ContactTable.COLUMN_CONTACT_EMAIL_IDX);

        if(Contact.avatar.equals("letter")){
            holder.checkBox.setVisibility(View.GONE);
            holder.letter.setVisibility(View.VISIBLE);
        }else if(Contact.avatar.equals("checkbox")){
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.letter.setVisibility(View.GONE);
        }

        holder.letter.setText(contact_name.charAt(0) + "");
        holder.name.setText(contact_name);
        holder.email.setText(contact_email);

        view.setId(ContactTable.COLUMN_CONTACT_ID_IDX);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_row_contacts, parent, false);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        return view;
    }

    public void updateView(View view, boolean checked){
        ViewHolder holder = new ViewHolder(view);

        holder.checkBox.setChecked(checked);
        notifyDataSetChanged();
    }

    public static class ViewHolder{

        TextView letter;
        CheckBox checkBox;
        TextView name;
        TextView email;

        public ViewHolder(View view){
            letter = (TextView) view.findViewById(R.id.contact_letter);
            checkBox = (CheckBox) view.findViewById(R.id.contact_checkbox);
            name = (TextView) view.findViewById(R.id.contact_name);
            email = (TextView) view.findViewById(R.id.contact_email);
        }

    }

}
