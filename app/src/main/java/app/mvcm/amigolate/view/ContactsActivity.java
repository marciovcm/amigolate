package app.mvcm.amigolate.view;

import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.zip.Inflater;

import app.mvcm.amigolate.R;
import app.mvcm.amigolate.view.adapter.ContactAdapter;
import app.mvcm.amigolate.controller.contentprovider.ContactProvider;
import app.mvcm.amigolate.controller.database.ContactTable;
import app.mvcm.amigolate.model.Contact;

public class ContactsActivity extends AppCompatActivity{

    private Cursor cursor;
    private ListView contact_list;
    private ContactAdapter contact_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        getSupportActionBar().setElevation(1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton contact_fab = (FloatingActionButton) findViewById(R.id.contacts_fab);
        contact_fab.setOnClickListener(onClickListener);

        updateView("letter");
        contact_list.setOnItemClickListener(onItemClickListener);
        contact_list.setMultiChoiceModeListener(multiChoiceModeListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        ParticipantDialog dialog = new ParticipantDialog();
        dialog.setCancelable(false);
        dialog.show(getFragmentManager(), "ParticipantDialog_create_contact");
        }
    };

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            cursor = getContentResolver().query(ContactProvider.CONTENT_URI, null, "_id = " + id, null, null);
            if(cursor != null && cursor.moveToFirst()){
                Contact contact = new Contact();
                contact.setId(id);
                contact.setName(cursor.getString(ContactTable.COLUMN_CONTACT_NAME_IDX));
                contact.setEmail(cursor.getString(ContactTable.COLUMN_CONTACT_EMAIL_IDX));
                ParticipantDialog dialog = new ParticipantDialog();
                dialog.setCancelable(false);
                dialog.show(getFragmentManager(), "ParticipantDialog_update_contact");
            }
        }
    };

    private AbsListView.MultiChoiceModeListener multiChoiceModeListener = new AbsListView.MultiChoiceModeListener() {

        ArrayList arrayList = new ArrayList();

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            View view = contact_list.getAdapter().getView(position, null, contact_list);
            cursor = getContentResolver().query(ContactProvider.CONTENT_URI, null, "_id = " + id, null, null);
            if(cursor.getCount() > 0){
                ContactAdapter adapter = new ContactAdapter(getApplicationContext(), cursor, false);
                adapter.updateView(view,checked);
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater mMenuInflater = getMenuInflater();
            mMenuInflater.inflate(R.menu.contacts_menu, menu);
            updateView("checkbox");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int action = item.getItemId();

            switch (action){
                case R.id.contact_delete:
                    for(int i=0; i<arrayList.size(); i++){
                        getContentResolver().delete(ContactProvider.CONTENT_URI, "_id = " + arrayList.get(i), null);
                    }
                    break;
                case R.id.contact_email:
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            updateView("letter");
            arrayList.clear();
        }
    };

    public void updateView(String action){
        Contact.avatar = action;
        cursor = getContentResolver().query(ContactProvider.CONTENT_URI, null, null, null, ContactTable.COLUMN_CONTACT_NAME + " ASC");
        contact_list = (ListView) findViewById(R.id.contacts_list);
        contact_adapter = new ContactAdapter(this, cursor, true);
        contact_list.setAdapter(contact_adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        cursor.close();
        super.onDestroy();
    }
}
