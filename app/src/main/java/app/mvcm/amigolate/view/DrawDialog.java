package app.mvcm.amigolate.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import app.mvcm.amigolate.R;

public class DrawDialog extends DialogFragment implements View.OnClickListener{

    private EditText et_title;
    private EditText et_summary;
    private Button bt_cancel;
    private Button bt_ok;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_newdraw, null);

        et_title = (EditText) view.findViewById(R.id.dialog_newdraw_title);
        et_summary = (EditText) view.findViewById(R.id.dialog_newdraw_summary);
        bt_cancel = (Button) view.findViewById(R.id.dialog_newdraw_cancel);
        bt_ok = (Button) view.findViewById(R.id.dialog_newdraw_ok);

        et_title.addTextChangedListener(mTextWatcher);

        bt_cancel.setOnClickListener(this);
        bt_ok.setOnClickListener(this);

        builder.setMessage(R.string.dialog_newdraw_title);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onClick(View v) {
        int action = v.getId();

        switch (action){
            case R.id.dialog_newdraw_cancel:
                dismiss();
                break;
            case R.id.dialog_newdraw_ok:
                dismiss();
                Intent intent = new Intent(getActivity(), NewDrawActivity.class);
                intent.putExtra("draw_name", et_title.getText().toString());
                intent.putExtra("draw_summary", et_summary.getText().toString());
                startActivity(intent);
                break;
        }
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void afterTextChanged(Editable s) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count > 0){
                bt_ok.setTextColor(getResources().getColor(R.color.colorAccent));
                bt_ok.setClickable(true);
                bt_ok.setEnabled(true);
            }else{
                bt_ok.setTextColor(getResources().getColor(R.color.hint_text_color));
                bt_ok.setClickable(false);
                bt_ok.setEnabled(false);
            }
        }
    };
}
